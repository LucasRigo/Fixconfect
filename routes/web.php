<?php

use App\Http\Controllers\OrdensController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function(){


    Route::group(['prefix'=>'processos', 'where'=>['id'=>'[0-9]+']], function(){

        Route::any('',              ['as'=>'processos', 'uses'=>'\App\Http\Controllers\ProcessosController@index']);
        Route::get('create',        ['as'=>'processos.create', 'uses'=>'\App\Http\Controllers\ProcessosController@create']);
        Route::post('store',        ['as'=>'processos.store', 'uses'=>'\App\Http\Controllers\ProcessosController@store']);
        Route::get('{id}/destroy',  ['as'=>'processos.destroy','uses'=>'\App\Http\Controllers\ProcessosController@destroy']);
        Route::get('{id}/edit',     ['as'=>'processos.edit', 'uses'=>'\App\Http\Controllers\ProcessosController@edit']);
        Route::put('{id}/update',   ['as'=>'processos.update','uses'=>'\App\Http\Controllers\ProcessosController@update']);
        Route::get('exportProcessosPDF',           ['as'=>'processos.exportProcessosPDF','uses'=>'\App\Http\Controllers\PdfController@exportProcessosPDF']);


    });


    Route::group(['prefix'=>'aparelhos', 'where'=>['id'=>'[0-9]+']], function(){

        Route::any('',              ['as'=>'aparelhos', 'uses'=>'\App\Http\Controllers\AparelhosController@index']);
        Route::get('create',        ['as'=>'aparelhos.create', 'uses'=>'\App\Http\Controllers\AparelhosController@create']);
        Route::post('store',        ['as'=>'aparelhos.store', 'uses'=>'\App\Http\Controllers\AparelhosController@store']);
        Route::get('{id}/destroy',  ['as'=>'aparelhos.destroy','uses'=>'\App\Http\Controllers\AparelhosController@destroy']);
        Route::get('{id}/edit',     ['as'=>'aparelhos.edit', 'uses'=>'\App\Http\Controllers\AparelhosController@edit']);
        Route::put('{id}/update',   ['as'=>'aparelhos.update','uses'=>'\App\Http\Controllers\AparelhosController@update']);
        Route::get('exportAparelhosPDF',           ['as'=>'aparelhos.exportAparelhosPDF','uses'=>'\App\Http\Controllers\PdfController@exportAparelhosPDF']);

    });



    Route::group(['prefix'=>'linhas', 'where'=>['id'=>'[0-9]+']], function(){

        Route::any('',              ['as'=>'linhas', 'uses'=>'\App\Http\Controllers\LinhasController@index']);
        Route::get('create',        ['as'=>'linhas.create', 'uses'=>'\App\Http\Controllers\LinhasController@create']);
        Route::post('store',        ['as'=>'linhas.store', 'uses'=>'\App\Http\Controllers\LinhasController@store']);
        Route::get('{id}/destroy',  ['as'=>'linhas.destroy','uses'=>'\App\Http\Controllers\LinhasController@destroy']);
        Route::get('{id}/edit',     ['as'=>'linhas.edit', 'uses'=>'\App\Http\Controllers\LinhasController@edit']);
        Route::put('{id}/update',   ['as'=>'linhas.update','uses'=>'\App\Http\Controllers\LinhasController@update']);
        Route::get('exportLinhasPDF',           ['as'=>'linhas.exportLinhasPDF','uses'=>'\App\Http\Controllers\PdfController@exportLinhasPDF']);


    });

    Route::group(['prefix'=>'terceirizados', 'where'=>['id'=>'[0-9]+']], function(){

        Route::any('',              ['as'=>'terceirizados', 'uses'=>'\App\Http\Controllers\TerceirizadosController@index']);
        Route::get('create',        ['as'=>'terceirizados.create', 'uses'=>'\App\Http\Controllers\TerceirizadosController@create']);
        Route::post('store',        ['as'=>'terceirizados.store', 'uses'=>'\App\Http\Controllers\TerceirizadosController@store']);
        Route::get('{id}/destroy',  ['as'=>'terceirizados.destroy','uses'=>'\App\Http\Controllers\TerceirizadosController@destroy']);
        Route::get('{id}/edit',     ['as'=>'terceirizados.edit', 'uses'=>'\App\Http\Controllers\TerceirizadosController@edit']);
        Route::put('{id}/update',   ['as'=>'terceirizados.update','uses'=>'\App\Http\Controllers\TerceirizadosController@update']);
        Route::get('exportTerceirizadosPDF',           ['as'=>'terceirizados.exportTerceirizadosPDF','uses'=>'\App\Http\Controllers\PdfController@exportTerceirizadosPDF']);

    });


    Route::group(['prefix'=>'cores', 'where'=>['id'=>'[0-9]+']], function(){

        Route::any('',              ['as'=>'cores', 'uses'=>'\App\Http\Controllers\CoresController@index']);
        Route::get('create',        ['as'=>'cores.create', 'uses'=>'\App\Http\Controllers\CoresController@create']);
        Route::post('store',        ['as'=>'cores.store', 'uses'=>'\App\Http\Controllers\CoresController@store']);
        Route::get('{id}/destroy',  ['as'=>'cores.destroy','uses'=>'\App\Http\Controllers\CoresController@destroy']);
        Route::get('{id}/edit',     ['as'=>'cores.edit', 'uses'=>'\App\Http\Controllers\CoresController@edit']);
        Route::put('{id}/update',   ['as'=>'cores.update','uses'=>'\App\Http\Controllers\CoresController@update']);
        Route::get('exportCoresPDF',           ['as'=>'cores.exportCoresPDF','uses'=>'\App\Http\Controllers\PdfController@exportCoresPDF']);

    });

    Route::group(['prefix'=>'modelos', 'where'=>['id'=>'[0-9]+']], function(){

        Route::any('',              ['as'=>'modelos', 'uses'=>'\App\Http\Controllers\ModelosController@index']);
        Route::get('create',        ['as'=>'modelos.create', 'uses'=>'\App\Http\Controllers\ModelosController@create']);
        Route::post('store',        ['as'=>'modelos.store', 'uses'=>'\App\Http\Controllers\ModelosController@store']);
        Route::get('{id}/destroy',  ['as'=>'modelos.destroy','uses'=>'\App\Http\Controllers\ModelosController@destroy']);
        Route::get('{id}/edit',     ['as'=>'modelos.edit', 'uses'=>'\App\Http\Controllers\ModelosController@edit']);
        Route::put('{id}/update',   ['as'=>'modelos.update','uses'=>'\App\Http\Controllers\ModelosController@update']);
        Route::get('exportModelosPDF',           ['as'=>'modelos.exportModelosPDF','uses'=>'\App\Http\Controllers\PdfController@exportModelosPDF']);

    });

    Route::group(['prefix'=>'ordens', 'where'=>['id'=>'[0-9]+']], function(){

        Route::get('',              ['as'=>'ordens', 'uses'=>'\App\Http\Controllers\OrdensController@index']);
        Route::get('create',        ['as'=>'ordens.create', 'uses'=>'\App\Http\Controllers\OrdensController@create']);
        Route::post('store',        ['as'=>'ordens.store', 'uses'=>'\App\Http\Controllers\OrdensController@store']);
        Route::get('{id}/destroy',  ['as'=>'ordens.destroy','uses'=>'\App\Http\Controllers\OrdensController@destroy']);
        Route::get('{id}/edit',     ['as'=>'ordens.edit', 'uses'=>'\App\Http\Controllers\OrdensController@edit']);
        Route::put('{id}/update',   ['as'=>'ordens.update','uses'=>'\App\Http\Controllers\OrdensController@update']);
        Route::get('export',        ['as'=>'ordens.export','uses'=>'\App\Http\Controllers\OrdensController@export']);
        Route::get('exportOrdensPDF',           ['as'=>'ordens.exportOrdensPDF','uses'=>'\App\Http\Controllers\PdfController@exportOrdensPDF']);

    });


    Route::group(['prefix'=>'retrabalhos', 'where'=>['id'=>'[0-9]+']], function(){

        Route::get('',              ['as'=>'retrabalhos', 'uses'=>'\App\Http\Controllers\RetrabalhosController@index']);
        Route::get('create',        ['as'=>'retrabalhos.create', 'uses'=>'\App\Http\Controllers\RetrabalhosController@create']);
        Route::post('store',        ['as'=>'retrabalhos.store', 'uses'=>'\App\Http\Controllers\RetrabalhosController@store']);
        Route::get('{id}/destroy',  ['as'=>'retrabalhos.destroy','uses'=>'\App\Http\Controllers\RetrabalhosController@destroy']);
        Route::get('{id}/edit',     ['as'=>'retrabalhos.edit', 'uses'=>'\App\Http\Controllers\RetrabalhosController@edit']);
        Route::put('{id}/update',   ['as'=>'retrabalhos.update','uses'=>'\App\Http\Controllers\RetrabalhosController@update']);
        Route::get('exportRetrabalhosPDF',           ['as'=>'retrabalhos.exportRetrabalhosPDF','uses'=>'\App\Http\Controllers\PdfController@exportRetrabalhosPDF']);

    });

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
