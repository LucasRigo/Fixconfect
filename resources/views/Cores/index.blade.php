@extends('layouts.default')

@section('content')
    <div class="col-md-12 text-center">
        <h1>Cores</h1>
    </div>

    {!! Form::open(['name'=>'form_name', 'route'=>'cores']) !!}
        <div class="sidebar-form">
            <div class="input-group">
                <input type="text" name="desc_filtro" class="form-control" style="width: 80% !important;" placeholder="Pesquisa...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    {!! Form::close() !!}
    <br>
    
    
    {{ $cores->links() }}


    <table class="table table-stripe table-bordered table-hover">
        <div class="col-md-12 text-left">
            <a href="{{route('cores.create', []) }}" class="btn btn-success">Novo</a>
            <a href="{{route('cores.exportCoresPDF', []) }}" class="btn btn-info">Gerar PDF</a>
        </div>

        <br>

        <thead>
            <th>Nome</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($cores as $cor)
                <tr>
                    <td>{{$cor->nome}}</td>
                    <td>

                        <a href="{{ route('cores.edit', ['id' => $cor->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{$cor->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
@stop

@section('table-delete')
"cores"
@endsection