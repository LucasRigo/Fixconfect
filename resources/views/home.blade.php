@extends('layouts.default')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Bem Vindo') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Você está logado') }}

                </div>
            </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <img src="vendor/adminlte/dist/img/logoTND.png" alt="Logo TN" width="472" height="470">

        </div>
    </div>
</div>
@endsection
