@extends('layouts.default')

@section('content')
    <div class="col-md-12 text-center">
        <h1>Linhas</h1>
    </div>


    {!! Form::open(['name'=>'form_name', 'route'=>'linhas']) !!}
        <div class="sidebar-form">
            <div class="input-group">
                <input type="text" name="desc_filtro" class="form-control" style="width: 80% !important;" placeholder="Pesquisa...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    {!! Form::close() !!}
    <br>
    
    
    {{ $linhas->links() }}


    <table class="table table-stripe table-bordered table-hover">
        <div class="col-md-12 text-left">
            <a href="{{route('linhas.create', []) }}" class="btn btn-success">Novo</a>
            <a href="{{route('linhas.exportLinhasPDF', []) }}" class="btn btn-info">Gerar PDF</a>
        </div>

        <br>

        <thead>
            <th>Tipo</th>
            <th>Cor</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($linhas as $linha)
                <tr>
                    <td>{{$linha->tipo}}</td>
                    <td>{{$linha->cor}}</td>
                    <td>

                        <a href="{{ route('linhas.edit', ['id' => $linha->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{$linha->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
@stop

@section('table-delete')
"linhas"
@endsection