<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Terceirizados</title>
    <style>
        #tabela{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        #tabela td,#tabela th{
            border: 1px solid #ddd;
            padding: 8px;
        }
        #tabela th{
            padding-top: 12px;
            padding-bottom: 12px;
            padding-left: 5px;
            text-align: center;
            background-color:cornflowerblue;
            color: white;
        }
        
    </style>
</head>

<body>
    <table id="tabela">
	    <thead>
            <th>Nome</th>
            <th>Telefone</th>
            <th>Endereço</th>
        </thead>
        <tbody>
            @foreach($terceirizados as $terceirizado)
                <tr>
                    <td>{{$terceirizado->nome}}</td>
                    <td>{{$terceirizado->telefone}}</td>
                    <td>{{$terceirizado->endereco}}</td>

                </tr>
            
            @endforeach
        </tbody>
    </table>
</body>
</html>




