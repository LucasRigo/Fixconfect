@extends('layouts.default')

@section('content')
    <div class="col-md-12 text-center">
        <h1>Aparelhos</h1>
    </div>

    {!! Form::open(['name'=>'form_name', 'route'=>'aparelhos']) !!}
        <div class="sidebar-form">
            <div class="input-group">
                <input type="text" name="desc_filtro" class="form-control" style="width: 80% !important;" placeholder="Pesquisa...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    {!! Form::close() !!}
    <br>
    
    
    {{ $aparelhos->links() }}

    <table class="table table-stripe table-bordered table-hover">
        <div class="col-md-12 text-left">
            <a href="{{route('aparelhos.create', []) }}" class="btn btn-success">Novo</a>
            <a href="{{route('aparelhos.exportAparelhosPDF', []) }}" class="btn btn-info">Gerar PDF</a>
        </div>

        <br>

        <thead>
            <th>Nome</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($aparelhos as $aparelho)
                <tr>
                    <td>{{$aparelho->nome}}</td>
                    <td>

                        <a href="{{ route('aparelhos.edit', ['id' => $aparelho->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{$aparelho->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
@stop

@section('table-delete')
"aparelhos"
@endsection