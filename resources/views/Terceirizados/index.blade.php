@extends('layouts.default')

@section('content')
    <div class="col-md-12 text-center">
        <h1>Terceirizados</h1>
    </div>


    {!! Form::open(['name'=>'form_name', 'route'=>'terceirizados']) !!}
        <div class="sidebar-form">
            <div class="input-group">
                <input type="text" name="desc_filtro" class="form-control" style="width: 80% !important;" placeholder="Pesquisa...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    {!! Form::close() !!}
    <br>
    
    
    {{ $terceirizados->links() }}



    <table class="table table-stripe table-bordered table-hover">
        <div class="col-md-12 text-left">
            <a href="{{route('terceirizados.create', []) }}" class="btn btn-success">Novo</a>
            <a href="{{route('terceirizados.exportTerceirizadosPDF', []) }}" class="btn btn-info">Gerar PDF</a>
        </div>

        <br>

        <thead>
            <th>Nome</th>
            <th>Telefone</th>
            <th>Endereço</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($terceirizados as $terceirizado)
                <tr>
                    <td>{{$terceirizado->nome}}</td>
                    <td>{{$terceirizado->telefone}}</td>
                    <td>{{$terceirizado->endereco}}</td>
                    <td>

                        <a href="{{ route('terceirizados.edit', ['id' => $terceirizado->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{$terceirizado->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
@stop

@section('table-delete')
"terceirizados"
@endsection