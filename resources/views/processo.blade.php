<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Processos</title>
    <style>
        #tabela{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        #tabela td,#tabela th{
            border: 1px solid #ddd;
            padding: 8px;
        }
        #tabela th{
            padding-top: 12px;
            padding-bottom: 12px;
            padding-left: 5px;
            text-align: center;
            background-color:cornflowerblue;
            color: white;
        }
        
    </style>
</head>

<body>
    <table id="tabela">
	    <thead>
            <th>Nome</th>
            <th>Tempo em segundos</th>
        </thead>
        <tbody>
            @foreach($processos as $processo)
                <tr>
                    <td>{{$processo->nome}}</td>
                    <td>{{$processo->tempo}} segundos</td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
</body>
</html>




