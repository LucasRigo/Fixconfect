@extends('layouts.default')

@section('content')
    <div class="col-md-12 text-center">
        <h1>Modelos</h1>
    </div>


    {!! Form::open(['name'=>'form_name', 'route'=>'modelos']) !!}
        <div class="sidebar-form">
            <div class="input-group">
                <input type="text" name="desc_filtro" class="form-control" style="width: 80% !important;" placeholder="Pesquisa...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    {!! Form::close() !!}
    <br>


    {{ $modelos->links() }}











    <table class="table table-stripe table-bordered table-hover">
        <div class="col-md-12 text-left">
            <a href="{{route('modelos.create', []) }}" class="btn btn-success">Novo</a>
            <a href="{{route('modelos.exportModelosPDF', []) }}" class="btn btn-info">Gerar PDF</a>
        </div>

        <br>

        <thead>
            <th>Referencia</th>
            <th>Cores</th>
            <th>Processos</th>
            <th>Tempo</th>
            <th>Ações</th>
        </thead>
        <tbody>
            
            @foreach($modelos as $modelo)
                <?php
                $total = 0;
            ?>

                <tr>
                    <td>{{$modelo->referencia}}</td>
                    <td>
                        @foreach($modelo->cores as $a)
                            <li>{{ $a->cor->nome }}</li>
                        @endforeach
                    </td>

                    <td>
                        @foreach($modelo->processos as $a)
                            <li>{{ $a->processo->nome }} - {{ $a->processo->tempo}} segundos
                                <?php
                                    $total = $total + $a->processo->tempo;
                                ?>
                            </li>
                            
                        @endforeach
                    </td>

                    <td> <?php echo $total . " segundos"; ?> </td>




                    <td>

                        
                        <a href="#" onclick="return ConfirmaExclusao({{$modelo->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
@stop

@section('table-delete')
"modelos"
@endsection