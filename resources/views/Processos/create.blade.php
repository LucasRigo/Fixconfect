@extends('adminlte::page')

@section('content')
    <h3>Novo Processo</h3>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::open(['route'=>'processos.store'])!!}
        <div class="form-group">
            {!! Form::label('nome', 'Nome') !!}
            {!! Form::text('nome', null, ['class'=>'form-control', 'placeholder'=>'Digite o nome do processo', 'required'])!!}
        </div>

        <div class="form-group">
            {!! Form::label('tempo', 'Tempo em segundos') !!}
            {!! Form::number('tempo', null, ['class'=>'form-control', 'placeholder'=>'Digite o tempo do processo', 'required'])!!}
        </div>

        <div class="form-group">
            {!! Form::submit('Criar Processo', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close()!!}
@stop