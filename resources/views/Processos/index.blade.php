@extends('layouts.default')

@section('content')
    <div class="col-md-12 text-center">
        <h1>Processos</h1>
    </div>


    {!! Form::open(['name'=>'form_name', 'route'=>'processos']) !!}
        <div class="sidebar-form">
            <div class="input-group">
                <input type="text" name="desc_filtro" class="form-control" style="width: 80% !important;" placeholder="Pesquisa...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-default"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    {!! Form::close() !!}
    <br>
    
    
    {{ $processos->links() }}


    <table class="table table-stripe table-bordered table-hover">
        <div class="col-md-12 text-left">
            <a href="{{route('processos.create', []) }}" class="btn btn-success">Novo</a>
            <a href="{{route('processos.exportProcessosPDF', []) }}" class="btn btn-info">Gerar PDF</a>
        </div>

        <br>

        <thead>
            <th>Nome</th>
            <th>Tempo em segundos</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($processos as $processo)
                <tr>
                    <td>{{$processo->nome}}</td>
                    <td>{{$processo->tempo}} segundos</td>
                    <td>

                        <a href="{{ route('processos.edit', ['id' => $processo->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{$processo->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
@stop

@section('table-delete')
"processos"
@endsection