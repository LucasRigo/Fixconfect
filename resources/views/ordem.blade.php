<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ordens</title>
    <style>
        #tabela{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        #tabela td,#tabela th{
            border: 1px solid #ddd;
            padding: 8px;
        }
        #tabela th{
            padding-top: 12px;
            padding-bottom: 12px;
            padding-left: 5px;
            text-align: center;
            background-color:cornflowerblue;
            color: white;
        }
        
    </style>
</head>

<body>
    <table id="tabela">
	    <thead>
            <th>ID</th>
            <th>Terceirizado</th>
            <th>Aparelhos</th>
            <th>Linhas</th>
            <th>Modelos</th>
        </thead>
        <tbody>

            @foreach($ordens as $ordem)
                <tr>
                    <td>{{$ordem->id}}</td>
                    <td>{{$ordem->terceirizado->nome}}</td>
                    <td>
                        @foreach($ordem->aparelhos as $a)
                            <li>{{ $a->aparelho->nome }}</li>
                        @endforeach
                    </td>
                    <td>
                        @foreach($ordem->linhas as $a)
                            <li>{{ $a->linha->cor }} - {{ $a->linha->tipo }}</li>
                        @endforeach
                    </td>
                    <td>
                        @foreach($ordem->modelos as $a)
                            <li>{{ $a->modelo->referencia }}</li>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>




