@extends('layouts.default')

@section('content')
    <div class="col-md-12 text-center">
        <h1>Ordens</h1>
    </div>

    <table class="table table-stripe table-bordered table-hover">
        <div class="col-md-12 text-left">
            <a href="{{route('ordens.create', []) }}" class="btn btn-success">Novo</a>
            <a href="{{route('ordens.exportOrdensPDF', []) }}" class="btn btn-info">Gerar PDF</a>
        </div>

        <br>

        <thead>
            <th>ID</th>
            <th>Terceirizado</th>
            <th>Aparelhos</th>
            <th>Linhas</th>
            <th>Modelos</th>
            <th>Ações</th>
        </thead>
        <tbody>
            <?php
                $totalOrdem = 0;
            ?>
            @foreach($ordens as $ordem)
                <?php
                    $total = 0;
                ?>

                <tr>
                    <td>{{$ordem->id}}</td>

                    <td>{{$ordem->terceirizado->nome}}</td>

                    <td>
                        @foreach($ordem->aparelhos as $a)
                            <li>{{ $a->aparelho->nome }}</li>
                        @endforeach
                    </td>

                    <td>
                        @foreach($ordem->linhas as $a)
                            <li>{{ $a->linha->cor }} - {{ $a->linha->tipo }}</li>
                        @endforeach
                    </td>

                    <td>
                        @foreach($ordem->modelos as $a)
                            <li>{{ $a->modelo->referencia }}</li>

                           



                        @endforeach
                    </td>



                    <td>

                        
                        <a href="#" onclick="return ConfirmaExclusao({{$ordem->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
@stop

@section('table-delete')
"ordens"
@endsection