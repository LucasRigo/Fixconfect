@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-header" style="background: lightgray">
            <h3>Nova Ordem</h3>

            @if($errors->any())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            
            {!! Form::open(['route'=>'ordens.store'])!!}
                <div class="form-group">
                    {!! Form::label('terceirizado_id', 'Terceirizado:') !!}
                    {!! Form::select('terceirizado_id',
                        \App\Models\Terceirizado::orderBy('nome')->pluck('nome', 'id')->toArray(),
                        null, ['class'=>'form-control', 'required'])!!}
                </div>

                <hr/>

                <h4>Aparelhos</h4>
                <div class="input_field_wrap"></div>
                <br>

                <button style="float:right" class="add_field_button btn btn-default">Adicionar Aparelho</button>

                <br>
                <hr/>


                <h4>Linhas</h4>
                <div class="input_field_wrap2"></div>
                <br>

                <button style="float:right" class="add_field_button2 btn btn-default">Adicionar Linha</button>

                <br>
                <hr/>


                <h4>Modelos</h4>
                <div class="input_field_wrap3"></div>
                <br>

                <button style="float:right" class="add_field_button3 btn btn-default">Adicionar Modelo</button>

                <br>
                <hr/>







                <div class="form-group">
                    {!! Form::submit('Criar Ordem', ['class'=>'btn btn-primary']) !!}
                    {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
                </div>
            {!! Form::close()!!}
        </div>
    </div>
@stop


@section('js')
    <script>
        $(document).ready(function(){
            var wrapper = $(".input_field_wrap");
            var add_button = $(".add_field_button");
            var x=0;
            $(add_button).click(function(e){
                x++;

                var newField = '<div><div style="width:94%; float: left" id="aparelho">{!! Form::select("aparelhos[]", \App\Models\Aparelho:: orderBy("nome")->pluck("nome", "id")->toArray(), null, ["class"=>"form-control", "required", "placeholder"=>"Selecione um aparelho"]) !!} </div><button type="button" class="remove field btn btn-danger btn-circle"><i class="fa fa-times"></button></div>';

                $(wrapper).append(newField);
            });
            $(wrapper).on("click",".remove_field", function(e){
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            });
        })
    </script>

    <script>
        $(document).ready(function(){
            var wrapper = $(".input_field_wrap2");
            var add_button = $(".add_field_button2");
            var x=0;
            $(add_button).click(function(e){
                x++;

                var newField = '<div>  <div style="width:94%; float: left" id="linha">{!! Form::select("linhas[]", \App\Models\Linha::select(DB::raw("CONCAT(cor, ' - ', tipo) AS nome"), 'id')->orderBy("nome")->pluck("nome", "id")->toArray(), null, ["class"=>"form-control", "required", "placeholder"=>"Selecione um linha"]) !!}       </div><button type="button" class="remove field btn btn-danger btn-circle"><i class="fa fa-times"></button></div>';

                $(wrapper).append(newField);
            });
            $(wrapper).on("click",".remove_field", function(e){
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            });
        })
    </script>


    <script>
        $(document).ready(function(){
            var wrapper = $(".input_field_wrap3");
            var add_button = $(".add_field_button3");
            var x=0;
            $(add_button).click(function(e){
                x++;

                var newField = '<div><div style="width:94%; float: left" id="modelo">{!! Form::select("modelos[]", \App\Models\Modelo:: orderBy("referencia")->pluck("referencia", "id")->toArray(), null, ["class"=>"form-control", "required", "placeholder"=>"Selecione um modelo"]) !!} </div><button type="button" class="remove field btn btn-danger btn-circle"><i class="fa fa-times"></button></div>';

                $(wrapper).append(newField);
            });
            $(wrapper).on("click",".remove_field", function(e){
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            });
        })
    </script>
@stop
