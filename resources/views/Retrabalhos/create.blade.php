
@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-header" style="background: lightgray">
            <h3>Novo Retrabalho</h3>

            @if($errors->any())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::open(['route'=>'retrabalhos.store'])!!}
                

                <div class="form-group">
                    {!! Form::label('ordem_id', 'Ordem:') !!}
                    {!! Form::select('ordem_id',
                        \App\Models\Ordem::orderBy('id')->pluck('id')->toArray(),
                        null, ['class'=>'form-control', 'required', 'placeholder'=>'Selecione a ordem'])!!}
                </div>



                <hr />

                <div class="form-group">
                    {!! Form::label('observacao', 'Observações') !!}
                    {!! Form::text('observacao', null, ['class'=>'form-control', 'placeholder'=>'Digite as observaçes','required'])!!}
                </div>

                                

                


                <div class="form-group">
                    {!! Form::submit('Criar Retrabalho', ['class'=>'btn btn-primary']) !!}
                    {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
                </div>
            {!! Form::close()!!}
        </div>
    </div>
@stop
