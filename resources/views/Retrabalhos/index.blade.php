@extends('layouts.default')

@section('content')
    <div class="col-md-12 text-center">
        <h1>Retrabalhos</h1>
    </div>

    <table class="table table-stripe table-bordered table-hover">
        <div class="col-md-12 text-left">
            <a href="{{route('retrabalhos.create', []) }}" class="btn btn-success">Novo</a>
            <a href="{{route('retrabalhos.exportRetrabalhosPDF', []) }}" class="btn btn-info">Gerar PDF</a>
        </div>

        <br>

        <thead>
            <th>Retrabalho ID</th>
            <th>ID da Ordem</th>
            <th>Observações</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($retrabalhos as $retrabalho)
                <tr>
                    <td>{{$retrabalho->id}}</td>

                    <td>{{$retrabalho->ordem_id}}</td>

                    <td>{{$retrabalho->observacao}}</td>

                    <td>

                        
                        <a href="#" onclick="return ConfirmaExclusao({{$retrabalho->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
@stop

@section('table-delete')
"retrabalhos"
@endsection