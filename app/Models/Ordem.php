<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ordem extends Model
{
    use HasFactory;

    protected $fillable = ['terceirizado_id'];

    public function terceirizado(){
        return $this->belongsTo("App\Models\Terceirizado");
    }

    public function aparelhos(){
        return $this->hasMany("App\Models\OrdemAparelho");
    }

    public function linhas(){
        return $this->hasMany("App\Models\OrdemLinha");
    }

    public function modelos(){
        return $this->hasMany("App\Models\OrdemModelo");
    }

    public function retrabalho(){
        return $this->hasMany("App\Models\Retrabalho");
    }
}
