<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdemAparelho extends Model
{
    use HasFactory;

    protected $table = "ordem_aparelhos";
    protected $fillable = ['ordem_id', 'aparelho_id'];

    public function aparelho(){
        return $this->belongsTo("App\Models\Aparelho");
    }
}
