<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdemModelo extends Model
{
    use HasFactory;

    protected $table = "ordem_modelos";
    protected $fillable = ['ordem_id', 'modelo_id'];

    public function modelo(){
        return $this->belongsTo("App\Models\Modelo");
    }
    
}
