<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdemLinha extends Model
{
    use HasFactory;

    protected $table = "ordem_linhas";
    protected $fillable = ['ordem_id', 'linha_id', 'quantidade'];

    public function linha(){
        return $this->belongsTo("App\Models\Linha");
    }
}
