<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Retrabalho extends Model
{
    use HasFactory;

    protected $fillable = ['ordem_id', 'observacao'];

    public function ordem(){
        return $this->belongsTo("App\Models\Ordem");
    }
}
