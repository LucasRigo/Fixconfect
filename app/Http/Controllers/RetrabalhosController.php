<?php

namespace App\Http\Controllers;

use App\Models\Retrabalho;
use App\Http\Requests\RetrabalhoRequest;

use Illuminate\Http\Request;

class RetrabalhosController extends Controller
{
    public function index(){
        $retrabalhos = Retrabalho::All();
        return view('retrabalhos.index', ['retrabalhos'=>$retrabalhos]);
    }

    public function create(){
        return view('retrabalhos.create');
    }

    public function store(RetrabalhoRequest $request){
        $retrabalho = Retrabalho::create([
            'ordem_id'=>$request->get('ordem_id'),
            'observacao'=>$request->get('observacao')

        ]);

        return redirect()->route('retrabalhos');
    }

    

    public function destroy($id){
        try{
            Retrabalho::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>"null");
        }catch(\Illuminate\Database\QueryException $e){
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        catch(\PDOException $e){
            $ret =array('status'=>500, 'msg'=>$e->getMessage());
        }
        return $ret;
    }

    public function edit($id){
        $retrabalho = Retrabalho::find($id);
        return view('retrabalhos.edit', compact('retrabalho'));
    }

    public function update(RetrabalhoRequest $request, $id){
        Retrabalho::find($id)->update($request->all());
        return redirect()->route('retrabalhos');
    }
}
