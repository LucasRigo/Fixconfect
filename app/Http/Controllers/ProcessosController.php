<?php

namespace App\Http\Controllers;

use App\Models\Processo;
use App\Http\Requests\ProcessoRequest;
use App\Http\Requests\Request;
use Symfony\Component\Process\Process;

class ProcessosController extends Controller
{
    public function index(Request $filtro){
        $filtragem = $filtro->get('desc_filtro');
        if($filtragem == null)
            $processos = Processo::orderBy('nome')->paginate('10');
        else
            $processos = Processo::where('nome', 'like', '%'.$filtragem.'%')
                ->orderBy("nome")
                ->paginate(10)
                ->setpath('processos?desc_filtro='.$filtragem);
        
        return view('processos.index', ['processos'=>$processos]);
    }

    public function create(){
        return view('processos.create');
    }

    public function store(ProcessoRequest $request){
        $novo_processo = $request->all();
        Processo::create($novo_processo);

        return redirect()->route('processos');
    }

    public function destroy($id){
        try{
            Processo::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>"null");
        } catch(\Illuminate\Database\QueryException $e){
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        catch(\PDOException $e){
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        
        return $ret;
    }

    public function edit($id){
        $processo = Processo::find($id);
        return view('processos.edit', compact('processo'));
    }

    public function update(ProcessoRequest $request, $id){
        Processo::find($id)->update($request->all());
        return redirect()->route('processos');
    }
}
