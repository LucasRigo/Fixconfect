<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ordem;
use App\Models\Modelo;
use App\Models\Retrabalho;
use App\Models\Terceirizado;
use App\Models\Aparelho;
use App\Models\Cor;
use App\Models\Linha;
use App\Models\Processo;
use PDF;

class PdfController extends Controller
{

    public function getAllOrdens()
    {
        $ordens = Ordem::all();
        return view('ordem', compact('ordens'));
    }



    public function exportOrdensPDF()
    {
        $ordens = Ordem::all();
        $pdf = PDF::loadView('ordem', compact('ordens'));
        return $pdf->download('ordens.pdf');
    }

    public function exportModelosPDF()
    {
        $modelos = Modelo::all();
        $pdf = PDF::loadView('modelo', compact('modelos'));
        return $pdf->download('modelos.pdf');
    }

    public function exportRetrabalhosPDF()
    {
        $retrabalhos = Retrabalho::all();
        $pdf = PDF::loadView('retrabalho', compact('retrabalhos'));
        return $pdf->download('retrabalhos.pdf');
    }

    public function exportTerceirizadosPDF()
    {
        $terceirizados = Terceirizado::all();
        $pdf = PDF::loadView('terceirizado', compact('terceirizados'));
        return $pdf->download('terceirizados.pdf');
    }

    public function exportAparelhosPDF()
    {
        $aparelhos = Aparelho::all();
        $pdf = PDF::loadView('aparelho', compact('aparelhos'));
        return $pdf->download('aparelhos.pdf');
    }

    public function exportCoresPDF()
    {
        $cores = Cor::all();
        $pdf = PDF::loadView('cor', compact('cores'));
        return $pdf->download('cores.pdf');
    }

    public function exportLinhasPDF()
    {
        $linhas = Linha::all();
        $pdf = PDF::loadView('linha', compact('linhas'));
        return $pdf->download('linhas.pdf');
    }

    public function exportProcessosPDF()
    {
        $processos = Processo::all();
        $pdf = PDF::loadView('processo', compact('processos'));
        return $pdf->download('processos.pdf');
    }
    

}