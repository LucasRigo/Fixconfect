<?php

namespace App\Http\Controllers;

use App\Models\Cor;
use App\Http\Requests\CorRequest;
use App\Http\Requests\Request;
use Symfony\Component\Process\Process;

class CoresController extends Controller
{
    public function index(Request $filtro){
        $filtragem = $filtro->get('desc_filtro');
        if($filtragem == null)
            $cores = Cor::orderBy('nome')->paginate('10');
        else
            $cores = Cor::where('nome', 'like', '%'.$filtragem.'%')
                ->orderBy("nome")
                ->paginate(10)
                ->setpath('cores?desc_filtro='.$filtragem);
        
        return view('cores.index', ['cores'=>$cores]);
    }

    public function create(){
        return view('cores.create');
    }

    public function store(CorRequest $request){
        $nova_cor = $request->all();
        Cor::create($nova_cor);

        return redirect()->route('cores');
    }

    public function destroy($id){
        try{
            Cor::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>"null");
        }catch(\Illuminate\Database\QueryException $e){
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        catch(\PDOException $e){
            $ret =array('status'=>500, 'msg'=>$e->getMessage());
        }
        return $ret;
    }

    public function edit($id){
        $cor = Cor::find($id);
        return view('cores.edit', compact('cor'));
    }

    public function update(CorRequest $request, $id){
        Cor::find($id)->update($request->all());
        return redirect()->route('cores');
    }
}
