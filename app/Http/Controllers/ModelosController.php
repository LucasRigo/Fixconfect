<?php

namespace App\Http\Controllers;

use App\Models\Modelo;
use App\Http\Requests\ModeloRequest;
use App\Models\ModeloCor;
use App\Models\ModeloProcesso;
use App\Models\Processo;
use App\Http\Requests\Request;
use Symfony\Component\Process\Process;
use DB;

class ModelosController extends Controller
{

    public function index(Request $filtro){
        $filtragem = $filtro->get('desc_filtro');
        if($filtragem == null)
            $modelos = Modelo::orderBy('referencia')->paginate('10');
        else
            $modelos = Modelo::where('referencia', 'like', '%'.$filtragem.'%')
                ->orderBy("referencia")
                ->paginate(10)
                ->setpath('modelos?desc_filtro='.$filtragem);
        
        return view('modelos.index', ['modelos'=>$modelos]);
    }




    public function create(){
        return view('modelos.create');
    }

    
    public function store(ModeloRequest $request){












        $modelo = Modelo::create([
            'referencia' => $request->get('referencia'),
        ]);

        $cores = $request->cores;
        foreach($cores as $a => $value) {
            ModeloCor::create([
                'modelo_id' => $modelo->id,
                'cor_id' => $cores[$a]
            ]);
        }

        
        
        $processos = $request->processos;
        foreach($processos as $a => $value) {
            ModeloProcesso::create([
                'modelo_id' => $modelo->id,
                'processo_id' => $processos[$a]
            ]);
        }


        

        



        return redirect()->route('modelos');
    }

    public function destroy($id){
        try{
            Modelo::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>"null");
        }catch(\Illuminate\Database\QueryException $e){
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        catch(\PDOException $e){
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        
        return $ret;
    }

    public function edit($id){
        $modelo = Modelo::find($id);
        return view('modelos.edit', compact('modelo'));
    }

    public function update(ModeloRequest $request, $id){
        Modelo::find($id)->update($request->all());
        return redirect()->route('modelos');
    }
}
