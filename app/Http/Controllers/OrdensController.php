<?php

namespace App\Http\Controllers;

use App\Models\Ordem;
use App\Http\Requests\OrdemRequest;
use App\Models\OrdemAparelho;
use App\Models\OrdemLinha;
use App\Models\OrdemModelo;
use Illuminate\Http\Request;
use PDF;

class OrdensController extends Controller
{
    public function index(){
        $ordens = Ordem::All();
        return view('ordens.index', ['ordens'=>$ordens]);
    }

    public function create(){
        return view('ordens.create');
    }

    public function store(OrdemRequest $request){
        $ordem = Ordem::create([
            'terceirizado_id'=>$request->get('terceirizado_id')
        ]);

        $aparelhos = $request->aparelhos;
        foreach($aparelhos as $a => $value){
            OrdemAparelho::create([
                'ordem_id' => $ordem->id,
                'aparelho_id' => $aparelhos[$a]
            ]);
        }

        $linhas = $request->linhas;
        foreach($linhas as $a => $value){
            OrdemLinha::create([
                'ordem_id' => $ordem->id,
                'linha_id' => $linhas[$a]
            ]);
        }

        $modelos = $request->modelos;
        foreach($modelos as $a => $value){
            OrdemModelo::create([
                'ordem_id' => $ordem->id,
                'modelo_id' => $modelos[$a]
            ]);
        }



        return redirect()->route('ordens');
    }

    public function destroy($id){
        try{
            Ordem::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>"null");
        }catch(\Illuminate\Database\QueryException $e){
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        catch(\PDOException $e){
            $ret =array('status'=>500, 'msg'=>$e->getMessage());
        }
        return $ret;
    }

    public function edit($id){
        $ordem = Ordem::find($id);
        return view('ordens.edit', compact('ordem'));
    }

    public function update(OrdemRequest $request, $id){
        Ordem::find($id)->update($request->all());
        return redirect()->route('ordens');
    }




}
