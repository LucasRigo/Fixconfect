<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdemLinhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordem_linhas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ordem_id')->unsigned()->nullable();
            $table->foreign('ordem_id')->references('id')->on('ordems')->onDelete('cascade');

            $table->bigInteger('linha_id')->unsigned()->nullable();
            $table->foreign('linha_id')->references('id')->on('linhas')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordem_linhas');
    }
}
