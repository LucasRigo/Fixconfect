<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdemAparelhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordem_aparelhos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ordem_id')->unsigned()->nullable();
            $table->foreign('ordem_id')->references('id')->on('ordems')->onDelete('cascade');
            $table->bigInteger('aparelho_id')->unsigned()->nullable();
            $table->foreign('aparelho_id')->references('id')->on('aparelhos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordem_aparelhos');
    }
}
