<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModeloCorsTable extends Migration
{
    //Aqui está criando a tabela com referencia, unindo modelo e cor no banco

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelo_cors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('modelo_id')->unsigned()->nullable();
            $table->foreign('modelo_id')->references('id')->on('modelos')->onDelete('cascade');

            $table->bigInteger('cor_id')->unsigned()->nullable();
            $table->foreign('cor_id')->references('id')->on('cors')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelo_cors');
    }
}
