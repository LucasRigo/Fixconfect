<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTerceirizadoIdToOrdems extends Migration
{
    //
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordems', function (Blueprint $table) {
            $table->bigInteger('terceirizado_id')->unsigned()->nullable();
            $table->foreign('terceirizado_id')->references('id')->on('terceirizados')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordems', function (Blueprint $table) {
            //
        });
    }
}
