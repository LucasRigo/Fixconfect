<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrdemIdToRetrabalhos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retrabalhos', function (Blueprint $table) {
            $table->bigInteger('ordem_id')->unsigned()->nullable();
            $table->foreign('ordem_id')->references('id')->on('ordems')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retrabalhos', function (Blueprint $table) {
            //
        });
    }
}
